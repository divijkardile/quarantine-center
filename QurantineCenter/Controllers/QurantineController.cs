﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QurantineCenter.CustomExceptions;
using QurantineCenter.Models;
using QurantineCenter.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QurantineCenter.Controllers
{
    [Route("api/[controller]")] //api/Qurantine
    [ApiController]
    public class QurantineController : ControllerBase
    {

        private IQurantineService _qurantineService;

        public QurantineController(IQurantineService qurantineService)
        {

            _qurantineService = qurantineService;
        }

        [HttpPost("addcenter")]
        public async Task<ActionResult<int>> AddCenter(Center center)
        {

            try
            {

                int centerId = await _qurantineService.AddQurantineCenter(center);
                return Created("", new { success = centerId });
            }
            catch(DublicatePlaceNameException ex)
            {

                return BadRequest(new { failed = ex.Message });
            }
            catch(Exception ex)
            {

                return BadRequest(new { failed = ex.Message });
            }
        }
        
        [HttpPost("addpatient")]
        public async Task<ActionResult<int>> AddPatient(Patient patient)
        {

            try
            {

                int centerId = await _qurantineService.AddPatient(patient);
                return Created("", new { success = centerId });
            }
            catch (InvalidAgeException ex)
            {

                return BadRequest(new { failed = ex.Message });
            }
            catch (Exception ex)
            {

                return BadRequest(new { failed = ex.Message });
            }
        }

        public ActionResult Index()
        {
            return BadRequest();
        }

        [HttpGet("getcenters")]
        public async Task<ActionResult<List<Center>>> GetCenter()
        {

            try
            {

                var centers = await _qurantineService.GetCenters();
                return Created("", new { success = centers });
            }
            catch (Exception ex)
            {

                return BadRequest(new { failed = ex.Message });
            }
        }

        [HttpPut("updatecenter")]
        public async Task<ActionResult> UpdateCenter(Center center)
        {

            try
            {

                await _qurantineService.UpdateCenterDetails(center);
                return Ok(new { success = "Updated Successfully" });
            }
            catch (Exception ex)
            {

                return BadRequest(new { failed = ex.Message });
            }
        }

        [HttpDelete("deletecenter")]
        public async Task<ActionResult> DeleteCenter(int id) // work on it for formBody
        {

            try
            {

                await _qurantineService.DeleteCenterDetails(id);
                return Ok(new { success = "Deleted Successfully" });
            }
            catch (Exception ex)
            {

                return BadRequest(new { failed = ex.Message });
            }
        }
    }
}
