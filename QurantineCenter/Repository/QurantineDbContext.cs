﻿using Microsoft.EntityFrameworkCore;
using QurantineCenter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QurantineCenter.Repository
{
    public class QurantineDbContext : DbContext
    {

        public QurantineDbContext()
        {

        }

        public QurantineDbContext(DbContextOptions<QurantineDbContext> opts) : base(opts)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            var center = modelBuilder.Entity<Center>();
            var patient = modelBuilder.Entity<Patient>();

            center.HasKey(c => c.CenterId);
            center.Property(c => c.CenterId)
                .UseIdentityColumn();

            center.Property(c => c.CenterPlace)
                .HasMaxLength(20)
                .IsRequired();

            center.HasMany(c => c.Patient).WithOne(p => p.Center).HasForeignKey(p => p.PatientId).IsRequired(true);

            patient.HasKey(p => p.PatientId);
            patient.Property(p => p.PatientId)
                .UseIdentityColumn();

            patient.Property(p => p.PatientName)
                .HasMaxLength(20)
                .IsRequired();

            patient.Property(p => p.PatientAge)
                .IsRequired();

            patient.HasOne(p => p.Center).WithMany(c => c.Patient).HasForeignKey(p => p.CenterId).IsRequired(true);
        }

        public DbSet<Center> Centers { get; set; }
        public DbSet<Patient> Patients { get; set; }
    }
}
