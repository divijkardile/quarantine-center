﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using QurantineCenter.CustomExceptions;
using QurantineCenter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QurantineCenter.Repository
{
    public class QurantineRepo : IQurantineRepo
    {

        int demo;

        private QurantineDbContext _qurantineDbContext;

        public QurantineRepo(QurantineDbContext qurantineDbContext)
        {

            _qurantineDbContext = qurantineDbContext;
        }

        public async Task<int> AddQurantineCenter(Center center)
        {
            try
            {

                if (QurantineCenterPlaceExists(center.CenterPlace))
                {

                    throw new DublicatePlaceNameException("place is already register");
                }

                _qurantineDbContext.Centers.Add(center);
                await _qurantineDbContext.SaveChangesAsync();

                return GetCenterId(center.CenterPlace);
            }
            catch(SqlCustomException ex)
            {

                throw new SqlCustomException("Can't connect to database", ex);
            }

        }

        private bool QurantineCenterPlaceExists(string centerPlace)
        {
            return _qurantineDbContext.Centers.Any(e => e.CenterPlace == centerPlace);
        }

        private int GetCenterId(string placeName)
        {

            return _qurantineDbContext.Centers.Where(c => c.CenterPlace == placeName).Select(c => c.CenterId).FirstOrDefault();
        }

        public async Task<int> AddPatient(Patient patient)
        {
            try
            {

                _qurantineDbContext.Patients.Add(patient);
                return await _qurantineDbContext.SaveChangesAsync();
            }
            catch(SqlException ex)
            {

                throw new SqlCustomException("Can't connect to database", ex);
            }
        }

        public async Task<List<Center>> GetCenters()
        {
            try
            {

                return await _qurantineDbContext.Centers.ToListAsync();
            }
            catch (SqlException ex)
            {

                throw new SqlCustomException("Can't connect to database", ex);
            }
        }

        public async Task UpdateCenterDetails(Center center)
        {
            try
            {
                _qurantineDbContext.Entry(center).State = EntityState.Modified;

                await _qurantineDbContext.SaveChangesAsync();
            }
            catch (SqlException ex)
            {

                throw new SqlCustomException("Can't connect to database", ex);
            }
        }

        public async Task DeleteCenterDetails(int id)
        {

            try
            {
                var center = await _qurantineDbContext.Centers.FindAsync(id);
                if (center == null)
                {
                    throw new DetailsNotFoundException("No data found in database!!!");
                }

                _qurantineDbContext.Centers.Remove(center);

                await _qurantineDbContext.SaveChangesAsync();
            }
            catch (SqlException ex)
            {

                throw new SqlCustomException("Can't connect to database", ex);
            }
        }
    }
}
