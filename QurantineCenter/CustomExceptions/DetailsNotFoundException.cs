﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QurantineCenter.CustomExceptions
{
    public class DetailsNotFoundException: Exception
    {

        public DetailsNotFoundException(string message): base(message)
        {

        }
    }
}
