﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QurantineCenter.CustomExceptions
{
    public class InvalidAgeException: Exception
    {

        public InvalidAgeException(string message): base(message)
        {

        }
    }
}
