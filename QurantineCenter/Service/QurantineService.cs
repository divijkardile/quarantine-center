﻿using QurantineCenter.CustomExceptions;
using QurantineCenter.Models;
using QurantineCenter.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QurantineCenter.Service
{
    public class QurantineService : IQurantineService
    {

        private IQurantineRepo _qurantineRepo;

        public QurantineService(IQurantineRepo qurantineRepo)
        {

            _qurantineRepo = qurantineRepo;
        }

        public async Task<int> AddPatient(Patient patient)
        {
              
            if(patient.PatientAge < 18)
            {

                throw new InvalidAgeException("Age should be greater than or equal to 18");
            }

            return await _qurantineRepo.AddPatient(patient);
        }

        public async Task<int> AddQurantineCenter(Center center)
        {

            return await _qurantineRepo.AddQurantineCenter(center);
        }

        public async Task DeleteCenterDetails(int id)
        {

            await _qurantineRepo.DeleteCenterDetails(id);
        }

        public async Task<List<Center>> GetCenters()
        {

            return await _qurantineRepo.GetCenters();
        }

        public async Task UpdateCenterDetails(Center center)
        {

            await _qurantineRepo.UpdateCenterDetails(center);
        }
    }
}
