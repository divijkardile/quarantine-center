﻿using QurantineCenter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QurantineCenter.Service
{
    public interface IQurantineService
    {

        Task<int> AddQurantineCenter(Center center);
        Task<int> AddPatient(Patient patient);
        Task<List<Center>> GetCenters();
        Task UpdateCenterDetails(Center center);
        Task DeleteCenterDetails(int id);
    }
}
